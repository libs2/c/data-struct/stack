//! \file
//! \addtogroup TEST
//! \brief Unit testing file built for Ceedling
//! \author Luke Paulson
//! \date July 2, 2020
//! \copyright The MIT License (MIT)
//! \version

#include "unity.h"

#include "stack.h"

StackChar_s* test_char_stack;
StackU8_s* test_u8_stack;
StackU16_s* test_u16_stack;
StackU32_s* test_u32_stack;
StackFloat_s* test_float_stack;
StackI8_s* test_i8_stack;
StackI16_s* test_i16_stack;
StackI32_s* test_i32_stack;

void setUp(void)
{
    test_char_stack = new_char_stack();
    test_u8_stack = new_u8_stack();
    test_u16_stack = new_u16_stack();
    test_u32_stack = new_u32_stack();
    test_float_stack = new_float_stack();
    test_i8_stack = new_i8_stack();
    test_i16_stack = new_i16_stack();
    test_i32_stack = new_i32_stack();
}

void tearDown(void)
{
    delete_stack(test_char_stack);
    delete_stack(test_u8_stack);
    delete_stack(test_u16_stack);
    delete_stack(test_u32_stack);
    delete_stack(test_float_stack);
    delete_stack(test_i8_stack);
    delete_stack(test_i16_stack);
    delete_stack(test_i32_stack);
}

//! \test Newly created stack will point to NULL when created
void test_new_stack_points_to_null(void){
    TEST_ASSERT_NULL(test_char_stack->top);
    TEST_ASSERT_NULL(test_u8_stack->top);
    TEST_ASSERT_NULL(test_u16_stack->top);  
    TEST_ASSERT_NULL(test_u32_stack->top);
    TEST_ASSERT_NULL(test_float_stack->top);
    TEST_ASSERT_NULL(test_i8_stack->top);
    TEST_ASSERT_NULL(test_i16_stack->top);
    TEST_ASSERT_NULL(test_i32_stack->top);
}


//! \test Newly created stack is empty
void test_is_new_stack_empty(void){
    TEST_ASSERT_TRUE(is_empty(test_char_stack));
    TEST_ASSERT_TRUE(is_empty(test_u8_stack));
    TEST_ASSERT_TRUE(is_empty(test_u16_stack));
    TEST_ASSERT_TRUE(is_empty(test_u32_stack));
    TEST_ASSERT_TRUE(is_empty(test_float_stack));
    TEST_ASSERT_TRUE(is_empty(test_i8_stack));
    TEST_ASSERT_TRUE(is_empty(test_i16_stack));
    TEST_ASSERT_TRUE(is_empty(test_i32_stack));
}


//! \test Top is not pointing to null after
void test_top_node_not_null_after_push(void){
    push(test_char_stack, 'c');
    TEST_ASSERT_NOT_NULL(test_char_stack->top);
    
    push(test_u8_stack, 12);
    TEST_ASSERT_NOT_NULL(test_u8_stack->top);
    
    push(test_u16_stack, 267);
    TEST_ASSERT_NOT_NULL(test_u16_stack->top);
    
    push(test_u32_stack, 38500);
    TEST_ASSERT_NOT_NULL(test_u32_stack->top);
    
    push(test_float_stack, 3.14);
    TEST_ASSERT_NOT_NULL(test_float_stack->top);
    
    push(test_i8_stack, -1);
    TEST_ASSERT_NOT_NULL(test_i8_stack->top);
    
    push(test_i16_stack, 200);
    TEST_ASSERT_NOT_NULL(test_i16_stack->top);
    
    push(test_i32_stack, -500);
    TEST_ASSERT_NOT_NULL(test_i32_stack->top);
}


//! \test Stack is not empty after pushing value
void test_is_not_empty_after_push(void){
    push(test_char_stack, 'c');
    TEST_ASSERT_FALSE(is_empty(test_char_stack));
    
    push(test_u8_stack, 12);
    TEST_ASSERT_FALSE(is_empty(test_u8_stack));
    
    push(test_u16_stack, 267);
    TEST_ASSERT_FALSE(is_empty(test_u16_stack));
    
    push(test_u32_stack, 38500);
    TEST_ASSERT_FALSE(is_empty(test_u32_stack));
    
    push(test_float_stack, 3.14);
    TEST_ASSERT_FALSE(is_empty(test_float_stack));
    
    push(test_i8_stack, -1);
    TEST_ASSERT_FALSE(is_empty(test_i8_stack));
    
    push(test_i16_stack, 200);
    TEST_ASSERT_FALSE(is_empty(test_i16_stack));
    
    push(test_i32_stack, -500);
    TEST_ASSERT_FALSE(is_empty(test_i32_stack));
}

//! \test Peek at pushed value
void test_peek_at_pushed_value(void){
    push(test_char_stack, 'c');
    TEST_ASSERT_EQUAL_CHAR(peek(test_char_stack), 'c');

    push(test_u8_stack, 12);
    TEST_ASSERT_EQUAL_UINT8(peek(test_u8_stack), 12);

    push(test_u16_stack, 267);
    TEST_ASSERT_EQUAL_UINT16(peek(test_u16_stack), 267);

    push(test_u32_stack, 38500);
    TEST_ASSERT_EQUAL_UINT32(peek(test_u32_stack), 38500);

    push(test_float_stack, 3.14);
    TEST_ASSERT_EQUAL_FLOAT(peek(test_float_stack), 3.14);

    push(test_i8_stack, -1);
    TEST_ASSERT_EQUAL_INT8(peek(test_i8_stack), -1);

    push(test_i16_stack, 200);
    TEST_ASSERT_EQUAL_INT16(peek(test_i16_stack), 200);

    push(test_i32_stack, -500);
    TEST_ASSERT_EQUAL_INT32(peek(test_i32_stack), -500);
}

//! \test Pop value
void test_pop_value(void){
    push(test_char_stack, 'c');
    TEST_ASSERT_EQUAL_CHAR(pop(test_char_stack), 'c');

    push(test_u8_stack, 12);
    TEST_ASSERT_EQUAL_UINT8(pop(test_u8_stack), 12);

    push(test_u16_stack, 267);
    TEST_ASSERT_EQUAL_UINT16(pop(test_u16_stack), 267);

    push(test_u32_stack, 38500);
    TEST_ASSERT_EQUAL_UINT32(pop(test_u32_stack), 38500);

    push(test_float_stack, 3.14);
    TEST_ASSERT_EQUAL_FLOAT(pop(test_float_stack), 3.14);

    push(test_i8_stack, -1);
    TEST_ASSERT_EQUAL_INT8(pop(test_i8_stack), -1);

    push(test_i16_stack, 200);
    TEST_ASSERT_EQUAL_INT16(pop(test_i16_stack), 200);

    push(test_i32_stack, -500);
    TEST_ASSERT_EQUAL_INT32(pop(test_i32_stack), -500);
}

//! \test Stack empty after one Push/Pop
void test_push_pop_empty_check(void){
    push(test_char_stack, 'c');
    pop(test_char_stack);
    TEST_ASSERT_TRUE(is_empty(test_char_stack));

    push(test_u8_stack, 12);
    pop(test_u8_stack);
    TEST_ASSERT_TRUE(is_empty(test_u8_stack));

    push(test_u16_stack, 267);
    pop(test_u16_stack);
    TEST_ASSERT_TRUE(is_empty(test_u16_stack));

    push(test_u32_stack, 38500);
    pop(test_u32_stack);
    TEST_ASSERT_TRUE(is_empty(test_u32_stack));

    push(test_float_stack, 3.14);
    pop(test_float_stack);
    TEST_ASSERT_TRUE(is_empty(test_float_stack));

    push(test_i8_stack, -1);
    pop(test_i8_stack);
    TEST_ASSERT_TRUE(is_empty(test_i8_stack));

    push(test_i16_stack, 200);
    pop(test_i16_stack);
    TEST_ASSERT_TRUE(is_empty(test_i16_stack));

    push(test_i32_stack, -500);
    pop(test_i32_stack);
    TEST_ASSERT_TRUE(is_empty(test_i32_stack));
}


//! \test Pop returns null on empty stack
void test_pop_return_null_on_empty_stack(void){
    TEST_ASSERT_EQUAL_CHAR(pop(test_char_stack), 0);
    TEST_ASSERT_EQUAL_UINT8(pop(test_u8_stack), 0);
    TEST_ASSERT_EQUAL_UINT16(pop(test_u16_stack), 0);
    TEST_ASSERT_EQUAL_UINT32(pop(test_u32_stack), 0);
    TEST_ASSERT_EQUAL_FLOAT(pop(test_float_stack), 0);
    TEST_ASSERT_EQUAL_INT8(pop(test_i8_stack), 0);
    TEST_ASSERT_EQUAL_INT16(pop(test_i16_stack), 0);
    TEST_ASSERT_EQUAL_INT32(pop(test_i32_stack), 0);
}


//! \test Peek returns null on empty stack
void test_peek_returns_null_on_empty_stack(void){
    TEST_ASSERT_EQUAL_CHAR(peek(test_char_stack), 0);
    TEST_ASSERT_EQUAL_UINT8(peek(test_u8_stack), 0);
    TEST_ASSERT_EQUAL_UINT16(peek(test_u16_stack), 0);
    TEST_ASSERT_EQUAL_UINT32(peek(test_u32_stack), 0);
    TEST_ASSERT_EQUAL_FLOAT(peek(test_float_stack), 0);
    TEST_ASSERT_EQUAL_INT8(peek(test_i8_stack), 0);
    TEST_ASSERT_EQUAL_INT16(peek(test_i16_stack), 0);
    TEST_ASSERT_EQUAL_INT32(peek(test_i32_stack), 0);
}


