//! \file
//! \addtogroup STACK
//! \brief  Simple stack library
//! \author Luke Paulson
//! \date July 2, 2020
//! \copyright The MIT License (MIT) 
//! \version

#ifndef STACK_H
#define STACK_H

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

/*! \name Macro Functions
    \param s Stack pointer
    \param v Value
*/
//! \{

#define pop(s) s->pop(s)                        //!< \ref pos "See Pop from Stack"
#define push(s,v) s->push(s,v)                  //!< \ref ps "See Push to Stack"
#define peek(s) s->peek(s)                      //!< \ref pes "See Peek at Stack"
#define is_empty(s) s->is_empty(s)              //!< \ref ie "See Is Stack Empty"
#define delete_stack(s) s->delete(s)            //!< \ref ds "See Destroy Stack"

//! \}

typedef struct StackChar_s StackChar_s;
typedef struct StackU8_s StackU8_s;
typedef struct StackU16_s StackU16_s;
typedef struct StackU32_s StackU32_s;
typedef struct StackFloat_s StackFloat_s;
typedef struct StackI8_s StackI8_s;
typedef struct StackI16_s StackI16_s;
typedef struct StackI32_s StackI32_s;

//! \name Destroy stack function pointer
//! \{
typedef void (*fptrDestroyStack_CHAR_t)(StackChar_s*); 
typedef void (*fptrDestroyStack_U8_t)(StackU8_s*);
typedef void (*fptrDestroyStack_U16_t)(StackU16_s*);
typedef void (*fptrDestroyStack_U32_t)(StackU32_s*);
typedef void (*fptrDestroyStack_FLOAT_t)(StackFloat_s*);
typedef void (*fptrDestroyStack_I8_t)(StackI8_s*);
typedef void (*fptrDestroyStack_I16_t)(StackI16_s*);
typedef void (*fptrDestroyStack_I32_t)(StackI32_s*);
//! \}

//! \name Push function pointer
//! \{
typedef void (*fptrPush_CHAR_t)(StackChar_s*, char);
typedef void (*fptrPush_U8_t)(StackU8_s*, uint8_t);
typedef void (*fptrPush_U16_t)(StackU16_s*, uint16_t);
typedef void (*fptrPush_U32_t)(StackU32_s*, uint32_t);
typedef void (*fptrPush_FLOAT_t)(StackFloat_s*, float);
typedef void (*fptrPush_I8_t)(StackI8_s*, int8_t);
typedef void (*fptrPush_I16_t)(StackI16_s*, int16_t);
typedef void (*fptrPush_I32_t)(StackI32_s*, int32_t);
//! \}

//! \name Pop function pointer
//! \{
typedef char (*fptrPop_CHAR_t)(StackChar_s*);
typedef uint8_t (*fptrPop_U8_t)(StackU8_s*);
typedef uint16_t (*fptrPop_U16_t)(StackU16_s*);   
typedef uint32_t (*fptrPop_U32_t)(StackU32_s*);      
typedef float (*fptrPop_FLOAT_t)(StackFloat_s*);   
typedef int8_t (*fptrPop_I8_t)(StackI8_s*);   
typedef int16_t (*fptrPop_I16_t)(StackI16_s*);   
typedef int32_t (*fptrPop_I32_t)(StackI32_s*);     
//! \}        

//! \name Peek function pointer
//! \{
typedef char (*fptrPeek_CHAR_t)(StackChar_s*);
typedef uint8_t (*fptrPeek_U8_t)(StackU8_s*);
typedef uint16_t (*fptrPeek_U16_t)(StackU16_s*);  
typedef uint32_t (*fptrPeek_U32_t)(StackU32_s*);  
typedef float (*fptrPeek_FLOAT_t)(StackFloat_s*);  
typedef int8_t (*fptrPeek_I8_t)(StackI8_s*);  
typedef int16_t (*fptrPeek_I16_t)(StackI16_s*);  
typedef int32_t (*fptrPeek_I32_t)(StackI32_s*);
//! \} 

//! \name Empty function pointer
//! \{
typedef bool (*fptrEmpty_CHAR_t)(StackChar_s*);
typedef bool (*fptrEmpty_U8_t)(StackU8_s*);
typedef bool (*fptrEmpty_U16_t)(StackU16_s*);
typedef bool (*fptrEmpty_U32_t)(StackU32_s*);
typedef bool (*fptrEmpty_FLOAT_t)(StackFloat_s*);
typedef bool (*fptrEmpty_I8_t)(StackI8_s*);
typedef bool (*fptrEmpty_I16_t)(StackI16_s*);
typedef bool (*fptrEmpty_I32_t)(StackI32_s*);
//! \}


//! \name Stack base structure
//! \{

//! Char Stack
struct StackChar_s{
  struct NodeChar_s* top;                               //!< Pointer to top of stack
  fptrDestroyStack_CHAR_t delete;                     //!< pointer to delete function
  fptrPush_CHAR_t push;                               //!< pointer to push function
  fptrPop_CHAR_t pop;                                 //!< pointer to pop function
  fptrPeek_CHAR_t peek;                               //!< pointer to peek function
  fptrEmpty_CHAR_t is_empty;                        //!< pointer to is_empty function
};

//! Unsinged 8bit Stack
struct StackU8_s{
  struct NodeU8_s* top;                               //!< Pointer to top of stack
  fptrDestroyStack_U8_t delete;                     //!< pointer to delete function
  fptrPush_U8_t push;                               //!< pointer to push function
  fptrPop_U8_t pop;                                 //!< pointer to pop function
  fptrPeek_U8_t peek;                               //!< pointer to peek function
  fptrEmpty_U8_t is_empty;                        //!< pointer to is_empty function
};

//! Unsigned 16bit integer stack
struct StackU16_s{
  struct NodeU16_s* top;                                //!< Pointer to top of stack     
  fptrDestroyStack_U16_t delete;                     //!< pointer to delete function  
  fptrPush_U16_t push;                               //!< pointer to push function    
  fptrPop_U16_t pop;                                 //!< pointer to pop function     
  fptrPeek_U16_t peek;                               //!< pointer to peek function
  fptrEmpty_U16_t is_empty;                        //!< pointer to is_empty function      
};

//! Unsinged 32bit Stack
struct StackU32_s{                                     
  struct NodeU32_s* top;                               //!< Pointer to top of stack                             
  fptrDestroyStack_U32_t delete;                    //!< pointer to delete function
  fptrPush_U32_t push;                              //!< pointer to push function  
  fptrPop_U32_t pop;                                //!< pointer to pop function   
  fptrPeek_U32_t peek;                              //!< pointer to peek function
  fptrEmpty_U32_t is_empty;                        //!< pointer to is_empty function
};

//! Float Stack
struct StackFloat_s{
  struct NodeFloat_s* top;                               //!< Pointer to top of stack             
  fptrDestroyStack_FLOAT_t delete;                    //!< pointer to delete function
  fptrPush_FLOAT_t push;                              //!< pointer to push function  
  fptrPop_FLOAT_t pop;                                //!< pointer to pop function   
  fptrPeek_FLOAT_t peek;                              //!< pointer to peek function
  fptrEmpty_FLOAT_t is_empty;                        //!< pointer to is_empty function  
};

//! Singed 8bit Stack
struct StackI8_s{
  struct NodeI8_s* top;                               //!< Pointer to top of stack   
  fptrDestroyStack_I8_t delete;                    //!< pointer to delete function
  fptrPush_I8_t push;                              //!< pointer to push function  
  fptrPop_I8_t pop;                                //!< pointer to pop function   
  fptrPeek_I8_t peek;                              //!< pointer to peek function
  fptrEmpty_I8_t is_empty;                        //!< pointer to is_empty function  
};

//! Singed 16bit Stack
struct StackI16_s{
  struct NodeI16_s* top;                               //!< Pointer to top of stack    
  fptrDestroyStack_I16_t delete;                    //!< pointer to delete function
  fptrPush_I16_t push;                              //!< pointer to push function  
  fptrPop_I16_t pop;                                //!< pointer to pop function   
  fptrPeek_I16_t peek;                              //!< pointer to peek function  
  fptrEmpty_I16_t is_empty;                        //!< pointer to is_empty function
};

//! Signed 32bit Stack
struct StackI32_s{                                         
  struct NodeI32_s* top;                              //!< Pointer to top of stack
  fptrDestroyStack_I32_t delete;                      //!< pointer to delete function
  fptrPush_I32_t push;                                //!< pointer to push function  
  fptrPop_I32_t pop;                                  //!< pointer to pop function   
  fptrPeek_I32_t peek;                                //!< pointer to peek function 
  fptrEmpty_I32_t is_empty;                        //!< pointer to is_empty function 
};
//! \}


//! \name Stack node structure
//! \{
  
//! Char node object to be place into the char stack
static struct NodeChar_s{
  struct NodeChar_s* next;                                //!< Pointer to next node
  char val;                                               //!< Value of node
}NodeChar_s;

//! Uint8 node object to be place into the uint8 stack
static struct NodeU8_s{
  struct NodeU8_s* next;                                  //!< Pointer to next node
  uint8_t val;                                            //!< Value of node
}Node_s;

//! Uint16 node object to be place into the uint16 stack
static struct NodeU16_s{
  struct NodeU16_s* next;                                 //!< Pointer to next node
  uint16_t val;                                           //!< Value of node
}NodeU16_s;

//! Uin32 node object to be place into the uint32 stack
static struct NodeU32_s{
  struct NodeU32_s* next;                                 //!< Pointer to next node
  uint32_t val;                                           //!< Value of node
}NodeU32_s;

//! Float node object to be place into the Float stack
static struct NodeFloat_s{
  struct NodeFloat_s* next;                               //!< Pointer to next node
  float val;                                              //!< Value of node
}NodeFloat_s;

//! Int8 node object to be place into the Int8 stack
static struct NodeI8_s{
  struct NodeI8_s* next;                                 //!< Pointer to next node
  int8_t val;                                            //!< Value of node
}NodeI8_s;

//! Int16 node object to be place into the Int16 stack
static struct NodeI16_s{
  struct NodeI16_s* next;                                //!< Pointer to next node
  int16_t val;                                           //!< Value of node
}NodeI16_s;

//! Int32 node object to be place into the Int32 stack
static struct NodeI32_s{
  struct NodeI32_s* next;                                //!< Pointer to next node
  int32_t val;                                           //!< Value of node
}NodeI32_s;
//! \}

/*! \name New Stack
    \brief Create a stack with nothing in it, a new node will be created when a
            new value is pushed into it.

    \return Pointer to new Stack Object
    \retval NULL failed to create stack
*/
//! \{
  
StackChar_s* new_char_stack(void);
StackU8_s* new_u8_stack(void);
StackU16_s* new_u16_stack(void);
StackU32_s* new_u32_stack(void);
StackFloat_s* new_float_stack(void);
StackI8_s* new_i8_stack(void);
StackI16_s* new_i16_stack(void);
StackI32_s* new_i32_stack(void);
//! \}

/*! \anchor nn
    \name New Node
    \brief Creat a new new node object that points to the Node address below it

    \param next Pointer to next Node Object
    \return Pointer to new Node object
    \retval NULL failed to create node
*/
//! \{
struct NodeChar_s* new_char_node(struct NodeChar_s* next);
struct NodeU8_s* new_u8_node(struct NodeU8_s* next);
struct NodeU16_s* new_u16_node(struct NodeU16_s* next);
struct NodeU32_s* new_u32_node(struct NodeU32_s* next);
struct NodeFloat_s* new_float_node(struct NodeFloat_s* next);
struct NodeI8_s* new_i8_node(struct NodeI8_s* next);
struct NodeI16_s* new_i16_node(struct NodeI16_s* next);
struct NodeI32_s* new_i32_node(struct NodeI32_s* next);
//! \}

/*! \anchor ds
    \name Destroy stack
    \brief Creat a new new node object that points to the Node address below it

    \param self Stack object to be destoryed
*/
//! \{
static void destroy_char_stack(StackChar_s* self);
static void destroy_u8_stack(StackU8_s* self);
static void destroy_u16_stack(StackU16_s* self);
static void destroy_u32_stack(StackU32_s* self);
static void destroy_float_stack(StackFloat_s* self);
static void destroy_i8_stack(StackI8_s* self);
static void destroy_i16_stack(StackI16_s* self);
static void destroy_i32_stack(StackI32_s* self);
//! \}                   

/*! \anchor ps
    \name Push to Stack
    \brief Create a new node and push the value into that node

    \param self Destination Stack
    \param val Value to push into stack
*/
//! \{
static void push_char(StackChar_s* self, char val);
static void push_u8(StackU8_s* self, uint8_t val);
static void push_u16(StackU16_s* self, uint16_t val);
static void push_u32(StackU32_s* self, uint32_t val);
static void push_float(StackFloat_s* self, float val);
static void push_i8(StackI8_s* self, int8_t val);
static void push_i16(StackI16_s* self, int16_t val);
static void push_i32(StackI32_s*self, int32_t val);
//! \}

/*! \anchor pos
    \name Pop from Stack
    \brief remove the node from the stack and return the value of that node
    \note An empty stack will return "0"

    \param self Stack object to pop data from
    \return value
*/
//! \{
static char pop_char(StackChar_s* self);
static uint8_t pop_u8(StackU8_s* self);
static uint16_t pop_u16(StackU16_s* self);
static uint32_t pop_u32(StackU32_s* self);
static float pop_float(StackFloat_s* self);
static int8_t pop_i8(StackI8_s* self);
static int16_t pop_i16(StackI16_s* self);
static int32_t pop_i32(StackI32_s* self);
//! \}

/*! \anchor pes
    \name Peek at Stack
    \brief Peek at the top of the stack, but do not remove value
    \note An empty stack will return "0"

    \param self Stack object to peek at
    \return value
*/
//! \{
static char peek_char(StackChar_s* self);
static uint8_t peek_u8(StackU8_s* self);
static uint16_t peek_u16(StackU16_s* self);
static uint32_t peek_u32(StackU32_s* self);
static float peek_float(StackFloat_s* self);
static int8_t peek_i8(StackI8_s* self);
static int16_t peek_i16(StackI16_s* self);
static int32_t peek_i32(StackI32_s* self);                       
//! \}

/*! \anchor ie
    \name Is Stack Empty
    \brief Check if the stack is empty

    \param self Stack object to check
    \retval true Stack is empty
*/
//! \{
static bool is_empty_char(StackChar_s* self);
static bool is_empty_u8(StackU8_s* self);
static bool is_empty_u16(StackU16_s* self);
static bool is_empty_u32(StackU32_s* self);
static bool is_empty_float(StackFloat_s* self);
static bool is_empty_i8(StackI8_s* self);
static bool is_empty_i16(StackI16_s* self);
static bool is_empty_i32(StackI32_s* self);
//! \}

#endif // STACK_H
