
// author Luke Paulson
// date July 2, 2020
// copyright The MIT License (MIT)
// version 

#include "stack.h"


// New Stack  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

StackChar_s* new_char_stack(void){
  StackChar_s* self = NULL;
  self = (StackChar_s*)malloc(sizeof(StackChar_s));
  
  //Ensure memory was allocated
  if(self == NULL){
    return NULL;
  }
  
  //Non-existane top node points to NULL
  self->top = NULL;

  //Attach function pointers
  self->delete = &destroy_char_stack;
  self->push = &push_char;
  self->pop = &pop_char;
  self->peek = &peek_char;
  self->is_empty = &is_empty_char;
  return self;
}

StackU8_s* new_u8_stack(void){
  StackU8_s* self = NULL;
  self = (StackU8_s*)malloc(sizeof(StackU8_s));
  
  //Ensure memory was allocated
  if(self == NULL){
    return NULL;
  }
  
  //Non-existane top node points to NULL
  self->top = NULL;

  //Attach function pointers
  self->delete = &destroy_u8_stack;
  self->push = &push_u8;
  self->pop = &pop_u8;
  self->peek = &peek_u8;
  self->is_empty = &is_empty_u8;
  return self;
}


StackU16_s* new_u16_stack(void){
  StackU16_s* self = NULL;
  self = (StackU16_s*)malloc(sizeof(StackU16_s));
  
  //Ensure memory was allocated
  if(self == NULL){
    return NULL;
  }
  
  //Non-existane top node points to NULL
  self->top = NULL;

  //Attach function pointers
  self->delete = &destroy_u16_stack;
  self->push = &push_u16;
  self->pop = &pop_u16;
  self->peek = &peek_u16;
  self->is_empty = &is_empty_u16;
  return self;
}


StackU32_s* new_u32_stack(void){
  StackU32_s* self = NULL;
  self = (StackU32_s*)malloc(sizeof(StackU32_s));
  
  //Ensure memory was allocated
  if(self == NULL){
    return NULL;
  }
  
  //Non-existane top node points to NULL
  self->top = NULL;

  //Attach function pointers
  self->delete = &destroy_u32_stack;
  self->push = &push_u32;
  self->pop = &pop_u32;
  self->peek = &peek_u32;
  self->is_empty = &is_empty_u32;
  return self;
}


StackFloat_s* new_float_stack(void){
  StackFloat_s* self = NULL;
  self = (StackFloat_s*)malloc(sizeof(StackFloat_s));
  
  //Ensure memory was allocated
  if(self == NULL){
    return NULL;
  }
  
  //Non-existane top node points to NULL
  self->top = NULL;

  //Attach function pointers
  self->delete = &destroy_float_stack;
  self->push = &push_float;
  self->pop = &pop_float;
  self->peek = &peek_float;
  self->is_empty = &is_empty_float;
  return self;
}


StackI8_s* new_i8_stack(void){
  StackI8_s* self = NULL;
  self = (StackI8_s*)malloc(sizeof(StackI8_s));
  
  //Ensure memory was allocated
  if(self == NULL){
    return NULL;
  }
  
  //Non-existane top node points to NULL
  self->top = NULL;

  //Attach function pointers
  self->delete = &destroy_i8_stack;
  self->push = &push_i8;
  self->pop = &pop_i8;
  self->peek = &peek_i8;
  self->is_empty = &is_empty_i8;
  return self;
}


StackI16_s* new_i16_stack(void){
  StackI16_s* self = NULL;
  self = (StackI16_s*)malloc(sizeof(StackI16_s));
  
  //Ensure memory was allocated
  if(self == NULL){
    return NULL;
  }
  
  //Non-existane top node points to NULL
  self->top = NULL;

  //Attach function pointers
  self->delete = &destroy_i16_stack;
  self->push = &push_i16;
  self->pop = &pop_i16;
  self->peek = &peek_i16;
  self->is_empty = &is_empty_i16;
  return self;
}


StackI32_s* new_i32_stack(void){
  StackI32_s* self = NULL;
  self = (StackI32_s*)malloc(sizeof(StackI32_s));
  
  //Ensure memory was allocated
  if(self == NULL){
    return NULL;
  }
  
  //Non-existane top node points to NULL
  self->top = NULL;

  //Attach function pointers
  self->delete = &destroy_i32_stack;
  self->push = &push_i32;
  self->pop = &pop_i32;
  self->peek = &peek_i32;
  self->is_empty = &is_empty_i32;
  return self;
}


//New Node  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

struct NodeChar_s* new_char_node(struct NodeChar_s* next){
  struct NodeChar_s* self = NULL;
  self =(struct NodeChar_s*)malloc(sizeof(struct NodeChar_s));
  
  //Ensure memory was allocated
  if(self == NULL){
    return NULL;
  }
  
  self->next = next;
  self->val = 0;
  return self;
}

struct NodeU8_s* new_u8_node(struct NodeU8_s* next){
  struct NodeU8_s* self = NULL;
  self =(struct NodeU8_s*)malloc(sizeof(struct NodeU8_s));
  
  //Ensure memory was allocated
  if(self == NULL){
    return NULL;
  }
  
  self->next = next;
  self->val = 0;
  return self;
}

struct NodeU16_s* new_u16_node(struct NodeU16_s* next){
  struct NodeU16_s* self = NULL;
  self =(struct NodeU16_s*)malloc(sizeof(struct NodeU16_s));
  
  //Ensure memory was allocated
  if(self == NULL){
    return NULL;
  }
  
  self->next = next;
  self->val = 0;
  return self;
}

struct NodeU32_s* new_u32_node(struct NodeU32_s* next){
  struct NodeU32_s* self = NULL;
  self =(struct NodeU32_s*)malloc(sizeof(struct NodeU32_s));
  
  //Ensure memory was allocated
  if(self == NULL){
    return NULL;
  }
  
  self->next = next;
  self->val = 0;
  return self;
}

struct NodeFloat_s* new_float_node(struct NodeFloat_s* next){
  struct NodeFloat_s* self = NULL;
  self =(struct NodeFloat_s*)malloc(sizeof(struct NodeFloat_s));
  
  //Ensure memory was allocated
  if(self == NULL){
    return NULL;
  }
  
  self->next = next;
  self->val = 0;
  return self;
}

struct NodeI8_s* new_i8_node(struct NodeI8_s* next){
  struct NodeI8_s* self = NULL;
  self =(struct NodeI8_s*)malloc(sizeof(struct NodeI8_s));
  
  //Ensure memory was allocated
  if(self == NULL){
    return NULL;
  }
  
  self->next = next;
  self->val = 0;
  return self;
}

struct NodeI16_s* new_i16_node(struct NodeI16_s* next){
  struct NodeI16_s* self = NULL;
  self =(struct NodeI16_s*)malloc(sizeof(struct NodeI16_s));
  
  //Ensure memory was allocated
  if(self == NULL){
    return NULL;
  }
  
  self->next = next;
  self->val = 0;
  return self;
}

struct NodeI32_s* new_i32_node(struct NodeI32_s* next){
  struct NodeI32_s* self = NULL;
  self =(struct NodeI32_s*)malloc(sizeof(struct NodeI32_s));
  
  //Ensure memory was allocated
  if(self == NULL){
    return NULL;
  }
  
  self->next = next;
  self->val = 0;
  return self;
}

// Destroy Stack  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void destroy_char_stack(StackChar_s* self){
  while(self->top != NULL){
    pop(self);
  }

  free(self);
  return;
}


void destroy_u8_stack(StackU8_s* self){
  while(self->top != NULL){
    pop(self);
  }

  free(self);
  return;
}


void destroy_u16_stack(StackU16_s* self){
  while(self->top != NULL){
    pop(self);
  }

  free(self);
  return;
}


void destroy_u32_stack(StackU32_s* self){
  while(self->top != NULL){
    pop(self);
  }

  free(self);
  return;
}


void destroy_float_stack(StackFloat_s* self){
  while(self->top != NULL){
    pop(self);
  }

  free(self);
  return;
}


void destroy_i8_stack(StackI8_s* self){
  while(self->top != NULL){
    pop(self);
  }

  free(self);
  return;
}


void destroy_i16_stack(StackI16_s* self){
  while(self->top != NULL){
    pop(self);
  }

  free(self);
  return;
}


void destroy_i32_stack(StackI32_s* self){
  while(self->top != NULL){
    pop(self);
  }

  free(self);
  return;
}



// Push To Stack  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void push_char(StackChar_s* self, char val){
  self->top = new_char_node(self->top);
  self->top->val = val;
  return;
}

void push_u8(StackU8_s* self, uint8_t val){
  self->top = new_u8_node(self->top);
  self->top->val = val;
  return;
}

void push_u16(StackU16_s* self, uint16_t val){
  self->top = new_u16_node(self->top);
  self->top->val = val;
  return;
}

void push_u32(StackU32_s* self, uint32_t val){
  self->top = new_u32_node(self->top);
  self->top->val = val;
  return;
}

void push_float(StackFloat_s* self, float val){
  self->top = new_float_node(self->top);
  self->top->val = val;
  return;
}

void push_i8(StackI8_s* self, int8_t val){
  self->top = new_i8_node(self->top);
  self->top->val = val;
  return;
}

void push_i16(StackI16_s* self, int16_t val){
  self->top = new_i16_node(self->top);
  self->top->val = val;
  return;
}

void push_i32(StackI32_s* self, int32_t val){
  self->top = new_i32_node(self->top);
  self->top->val = val;
  return;
}

//  Pop From Stack  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

char pop_char(StackChar_s* self){
  if(is_empty_char(self) == true){
    return 0;
  }
  char temp_val;
  struct NodeChar_s* temp_node;

  //assign temp values
  temp_val = self->top->val;
  temp_node = self->top->next;

  //free top node and assign next node to top
  free(self->top);
  self->top = temp_node;
  
  return temp_val;
}

uint8_t pop_u8(StackU8_s* self){
  if(is_empty_u8(self) == true){
    return 0;
  }
  uint8_t temp_val;
  struct NodeU8_s* temp_node;

  //assign temp values
  temp_val = self->top->val;
  temp_node = self->top->next;

  //free top node and assign next node to top
  free(self->top);
  self->top = temp_node;
  
  return temp_val;
}

uint16_t pop_u16(StackU16_s* self){
  if(is_empty_u16(self) == true){
    return 0;
  }
  uint16_t temp_val;
  struct NodeU16_s* temp_node;

  //assign temp values
  temp_val = self->top->val;
  temp_node = self->top->next;

  //free top node and assign next node to top
  free(self->top);
  self->top = temp_node;
  
  return temp_val;
}

uint32_t pop_u32(StackU32_s* self){
  if(is_empty_u32(self) == true){
    return 0;
  }
  uint32_t temp_val;
  struct NodeU32_s* temp_node;

  //assign temp values
  temp_val = self->top->val;
  temp_node = self->top->next;

  //free top node and assign next node to top
  free(self->top);
  self->top = temp_node;
  
  return temp_val;
}

float pop_float(StackFloat_s* self){
  if(is_empty_float(self) == true){
    return 0;
  }
  float temp_val;
  struct NodeFloat_s* temp_node;

  //assign temp values
  temp_val = self->top->val;
  temp_node = self->top->next;

  //free top node and assign next node to top
  free(self->top);
  self->top = temp_node;
  
  return temp_val;
}

int8_t pop_i8(StackI8_s* self){
  if(is_empty_i8(self) == true){
    return 0;
  }
  int8_t temp_val;
  struct NodeI8_s* temp_node;

  //assign temp values
  temp_val = self->top->val;
  temp_node = self->top->next;

  //free top node and assign next node to top
  free(self->top);
  self->top = temp_node;
  
  return temp_val;
}


int16_t pop_i16(StackI16_s* self){
  if(is_empty_i16(self) == true){
    return 0;
  }
  int16_t temp_val;
  struct NodeI16_s* temp_node;

  //assign temp values
  temp_val = self->top->val;
  temp_node = self->top->next;

  //free top node and assign next node to top
  free(self->top);
  self->top = temp_node;
  
  return temp_val;
}


int32_t pop_i32(StackI32_s* self){
  if(is_empty_i32(self) == true){
    return 0;
  }
  int32_t temp_val;
  struct NodeI32_s* temp_node;

  //assign temp values
  temp_val = self->top->val;
  temp_node = self->top->next;

  //free top node and assign next node to top
  free(self->top);
  self->top = temp_node;
  
  return temp_val;
}

// Peek at Stack  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

char peek_char(StackChar_s* self){
  if(is_empty_char(self) == true){
    return 0;
  }
  return self->top->val;
}

uint8_t peek_u8(StackU8_s* self){
  if(is_empty_u8(self) == true){
    return 0;
  }
  return self->top->val;
}

uint16_t peek_u16(StackU16_s* self){
  if(is_empty_u16(self) == true){
    return 0;
  }
  return self->top->val;
}

uint32_t peek_u32(StackU32_s* self){
  if(is_empty_u32(self) == true){
    return 0;
  }
  return self->top->val;
}

float peek_float(StackFloat_s* self){
  if(is_empty_float(self) == true){
    return 0;
  }
  return self->top->val;
}

int8_t peek_i8(StackI8_s* self){
  if(is_empty_i8(self) == true){
    return 0;
  }
  return self->top->val;
}

int16_t peek_i16(StackI16_s* self){
  if(is_empty_i16(self) == true){
    return 0;
  }
  return self->top->val;
}

int32_t peek_i32(StackI32_s* self){
  if(is_empty_i32(self) == true){
    return 0;
  }
  return self->top->val;
}

// Is Stack Empty +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

bool is_empty_char(StackChar_s* self){
  if(self->top == NULL){
    return true;
  }
  return false;
}


bool is_empty_u8(StackU8_s* self){
  if(self->top == NULL){
    return true;
  }
  return false;
}


bool is_empty_u16(StackU16_s* self){
  if(self->top == NULL){
    return true;
  }
  return false;
}


bool is_empty_u32(StackU32_s* self){
  if(self->top == NULL){
    return true;
  }
  return false;
}


bool is_empty_float(StackFloat_s* self){
  if(self->top == NULL){
    return true;
  }
  return false;
}


bool is_empty_i8(StackI8_s* self){
  if(self->top == NULL){
    return true;
  }
  return false;
}


bool is_empty_i16(StackI16_s* self){
  if(self->top == NULL){
    return true;
  }
  return false;
}


bool is_empty_i32(StackI32_s* self){
  if(self->top == NULL){
    return true;
  }
  return false;
}